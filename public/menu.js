var menuState = {
    create: function() {
        // // Add a background image
        this.background = game.add.tileSprite(40, 64, 384, 354, 'background');
        this.leftWall = game.add.tileSprite(24, 64, 16, 354, 'wall');
        this.rightWall = game.add.tileSprite(424, 64, 16, 354, 'wall');
        this.backgroundBig = game.add.image(0, 0, 'backgroundBig');

        // // Display the name of the game
        var nameLabel = game.add.text(232, 100, 'NS-SHAFT',
        { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);

        // // Show the score at the center of the screen
        var scoreLabel = game.add.text(232, game.height/2+30,'loading...' , { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
    
        var database = firebase.database().ref('/scoreboard').orderByChild('nscore').limitToFirst(5);
        database.once('value', function(snapshot){
            var scoreText = '';
            snapshot.forEach(function(childSnapshot){
                scoreText += childSnapshot.val().name + '  ' + childSnapshot.val().score + '\n';
            });
            scoreLabel.text = scoreText;
        });

        game.global.player2Enable = false;

        var start_label = game.add.text(112, 360, '1 player', { font: '24px Arial', fill: '#fff' });
        start_label.inputEnabled = true;
        start_label.events.onInputUp.add(this.start);

        var player2_label = game.add.text(252, 360, '2 players', { font: '24px Arial', fill: '#fff' });
        player2_label.inputEnabled = true;
        player2_label.events.onInputUp.add(this.p2);
        
    },

    update: function() {
        this.background.tilePosition.y -= 0.4;
        this.leftWall.tilePosition.y -= 1;
        this.rightWall.tilePosition.y -= 1;
        console.log(game.global.player2Enable);
    },

    start: function() {
        // // Start the actual game
        game.state.start('play');
    },

    p2: function(){
        game.global.player2Enable = true;
        game.state.start('play');
    }
}; 