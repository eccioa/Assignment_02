var bootState = {
    preload: function () {
        // Load the progress bar image.
        game.load.image('healthbar', 'assets/healthbar.png');
    },

    create: function() {
        // Set some game settings.
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.stage.backgroundColor = '#000000';
        game.renderer.renderSession.roundPixels = true;
        // Start the load state.
        game.state.start('load');
    }
}; 